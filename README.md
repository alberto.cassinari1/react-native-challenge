# React Native Challenge
The purpose of this challenge is to create a tab bar navigation menu with also a lateral navigation menu (Drawer navigation).
In order to achive this, the navigation system must be created using React Navigation, here you'll be able to find the documentation:<br>
[Documentation](https://reactnavigation.org/docs/getting-started)<br><br>
On Figma you will be able to see the design of the above elements: <br>
[Figma Design](https://www.figma.com/file/YkvkChiRIIUxk8nNVaAwPf/BSamply-Challenge?node-id=0%3A1)<br><br>
The assets (since the icon of the active tab needs to change its color) are all available in the folder 'assets'<br>
The first icon, when pressed, needs to open the Drawer menu<br>
The images must use the svg format<br>
The content of the views for each tab is not relevant<br>
The application needs to run both on iOS and Android, if you are not using a Mac right now, add a compiling note file with the steps to compile also for iOS.
Once done open a new private repo and send the link for the code review.

Good luck and have fun
